using System;
using ChatSharp;
using System.Linq;
using System.Collections.Generic;
using ChatSharp.Events;
using System.Diagnostics;
using System.Threading;
using System.Threading.Tasks;
using System.IO;
using Mono.CSharp;

namespace Sirvant
{
	public class BotController
	{
		public IrcClient PrimaryClient { get; set; }
		public List<IrcClient> SecondaryClients { get; set; }
		public Dictionary<string, IrcUser> NickMaps { get; set; }
		public Dictionary<string, StreamWriter> Logs { get; set; }
        public bool Active { get; set; }
        public Timer Timer { get; set; }
        public Evaluator Evaluator { get; set; }

		public BotController()
		{
            Active = true;
            Evaluator = new Evaluator(new CompilerContext(new CompilerSettings(), new ConsoleReportPrinter()));
            Evaluator.ReferenceAssembly(typeof(BotController).Assembly);
            Evaluator.ReferenceAssembly(typeof(IrcClient).Assembly);
			SecondaryClients = new List<IrcClient>();
			NickMaps = new Dictionary<string, IrcUser>();
			PrimaryClient = new IrcClient(Program.Configuration.IrcServer, new IrcUser(
				Program.Configuration.Identity.Nick, Program.Configuration.Identity.User, Program.Configuration.Identity.Password));
            Evaluator.Evaluate("var client = Program.Controller.PrimaryClient");
			Logs = new Dictionary<string, StreamWriter>();
			SetUpPrimaryEventHandlers();
		}

		public void Run()
		{
			PrimaryClient.ConnectAsync();
		}

		private void SetUpPrimaryEventHandlers()
		{
			PrimaryClient.ConnectionComplete += HandleConnectionComplete;
			PrimaryClient.UserJoinedChannel += HandleUserJoinedChannel;
			PrimaryClient.ModeChanged += HandleModeChanged;
			PrimaryClient.ChannelMessageRecieved += HandleChannelMessageRecieved;
			PrimaryClient.PrivateMessageRecieved += HandlePrivateMessageRecieved;
			PrimaryClient.ChannelListRecieved += HandleChannelListRecieved;
			PrimaryClient.UserPartedChannel += HandleUserPartedChannel;
			PrimaryClient.UserKicked += HandleUserKicked;
			Logs[PrimaryClient.User.Nick] = new StreamWriter(PrimaryClient.User.Nick + ".log");
			PrimaryClient.RawMessageRecieved += (sender, e) => { Logs[(sender as IrcClient).User.Nick].WriteLine("<- " + e.Message); Logs[(sender as IrcClient).User.Nick].Flush(); };
			PrimaryClient.RawMessageSent += (sender, e) => { Logs[(sender as IrcClient).User.Nick].WriteLine("-> " + e.Message); Logs[(sender as IrcClient).User.Nick].Flush(); };
		}

		void HandleChannelListRecieved(object sender, ChannelEventArgs e)
		{
			Task.Factory.StartNew(() =>
            {
                Thread.Sleep(3000);
                var autoReset = new AutoResetEvent(false);
                for (int i = 0; i < e.Channel.Users.Count(); i++)
                {
                    var user = e.Channel.Users[i];
                    if (user.Hostname != null)
                        continue;
					if (NickMaps.ContainsKey(user.Nick))
						continue;
                    autoReset.Reset();
                    PrimaryClient.WhoIs(user.Nick, whois =>
                    {
						if (whois != null && whois.User != null && whois.User.Nick != null)
							NickMaps[whois.User.Nick] = whois.User;
                        autoReset.Set();
                    });
                    autoReset.WaitOne();
                    Thread.Sleep(1000);
                }
				Console.WriteLine("WHOIS complete");
            });
		}

		void HandlePrivateMessageRecieved(object sender, PrivateMessageEventArgs e)
		{
			if (Program.Configuration.TrustedMasks.Any(m => e.PrivateMessage.User.Match(m)))
				HandleTrustedCommand(e.PrivateMessage.Message, e.PrivateMessage, sender as IrcClient);
		}

		void HandleChannelMessageRecieved(object sender, PrivateMessageEventArgs e)
		{
			var client = sender as IrcClient;
			if (Program.Configuration.TrustedMasks.Any(m => e.PrivateMessage.User.Match(m)))
			{
				var message = e.PrivateMessage.Message;
				bool command = false;
				if (e.PrivateMessage.Message.StartsWith(client.User.Nick + ": "))
				{
					message = message.Substring(client.User.Nick.Length + 2);
					command = true;
				}
				if (e.PrivateMessage.Message.StartsWith(".") && client == PrimaryClient)
				{
					message = message.Substring(1);
					command = true;
				}
				if (command)
					HandleTrustedCommand(message, e.PrivateMessage, client);
			}
		}

		void HandleTrustedCommand(string message, PrivateMessage originalMessage, IrcClient target)
		{
			var command = message;
			var parameters = new string[0];
			if (message.Contains(" "))
			{
				command = message.Remove(message.IndexOf(' '));
				parameters = message.Substring(message.IndexOf(' ') + 1).Split(' ');
			}
			switch (command)
			{
				case "addchannel":
					bool multiclient = false;
					if (parameters.Length < 1 || parameters.Length > 2)
						break;
					if (parameters.Length == 2)
					{
						if (!bool.TryParse(parameters[1], out multiclient))
							break;
					}
					Program.Configuration.Channels = Program.Configuration.Channels.Concat(new[] { new ChannelDescriptor
						{
							Name = parameters[0],
							EnableMultiClient = multiclient
						} }).ToArray();
					Program.SaveConfiguration();
					PrimaryClient.JoinChannel(parameters[0]);
					if (multiclient)
					{
						foreach (var client in SecondaryClients)
							client.JoinChannel(parameters[0]);
					}
					break;
				case "removechannel":
					if (originalMessage.IsChannelMessage)
					{
						PrimaryClient.PartChannel(originalMessage.Source);
						foreach (var client in SecondaryClients)
							client.PartChannel(originalMessage.Source);
						Program.Configuration.Channels = Program.Configuration.Channels.Where(c => c.Name != originalMessage.Source).ToArray();
						Program.SaveConfiguration();
					}
					break;
				case "addclients":
					int addAmount;
					if (parameters.Length != 1)
						break;
					if (!int.TryParse(parameters[0], out addAmount))
						break;
					// Add that many clients
					var oldAmount = Program.Configuration.AlternateClients;
					Program.Configuration.AlternateClients += addAmount;
					Program.SaveConfiguration();
					int newAmount = SecondaryClients.Count + addAmount;
					for (int i = oldAmount + 1; i <= newAmount; i++)
					{
						var newClient = new IrcClient(Program.Configuration.IrcServer,new IrcUser(
							Program.Configuration.Identity.Nick + i, Program.Configuration.Identity.User, Program.Configuration.Identity.Password));
						SetUpSecondaryEventHandlers(newClient);
						SecondaryClients.Add(newClient);
						newClient.ConnectAsync();
					}
					break;
				case "removeclients":
					int removeAmount;
					if (parameters.Length != 1)
						break;
					if (!int.TryParse(parameters[0], out removeAmount))
						break;
					if (SecondaryClients.Count < removeAmount)
						break;
					// Remove a few clients
					Program.Configuration.AlternateClients -= removeAmount;
					Program.SaveConfiguration();
					for (int i = 0; i < removeAmount; i++)
					{
						SecondaryClients.Last().Quit("Requested by " + originalMessage.User.Nick);
						SecondaryClients.RemoveAt(SecondaryClients.Count - 1);
					}
					break;
				case "killall":
					PrimaryClient.Quit("Requested by " + originalMessage.User.Nick);
					foreach (var client in SecondaryClients)
						client.Quit("Requested by " + originalMessage.User.Nick);
					Process.GetCurrentProcess().Kill();
					while (true);
				case "listclients":
					var clients = string.Join(", ", new[] { PrimaryClient.User.Nick }.Concat(
						SecondaryClients.Select(c => c.User.Nick)).ToArray());
					PrimaryClient.SendMessage(clients, originalMessage.Source);
					break;
				case "quit":
					target.Quit("Requested by " + originalMessage.User.Nick);
					SecondaryClients.Remove(target);
					break;
				case "join":
					if (parameters.Length != 1)
						break;
					target.JoinChannel(parameters[0]);
					break;
                case "suspend":
                    Program.Configuration.RespondAggressively = false;
                    Program.SaveConfiguration();
                    PrimaryClient.SendMessage("Bot operation suspended.", originalMessage.Source);
                    break;
                case "resume":
                    Program.Configuration.RespondAggressively = true;
                    Program.SaveConfiguration();
                    PrimaryClient.SendMessage("Bot operation resumed.", originalMessage.Source);
                    break;
                case "raw":
                    PrimaryClient.SendRawMessage(message.Substring(4));
                    break;
                case "rawall":
                    var raw = message.Substring(7);
                    PrimaryClient.SendRawMessage(raw);
                    foreach (var client in SecondaryClients)
                        client.SendRawMessage(raw);
                    break;
                case "timer":
                    int period = int.Parse(parameters[0]);
                    var timedCommand = string.Join(" ", parameters.Skip(1).ToArray());
                    PrimaryClient.SendMessage(string.Format("Repeating '{0}' every {1}ms", timedCommand, period), originalMessage.Source);
                    Timer = new Timer(o => 
                    {
                        HandleTrustedCommand(timedCommand, originalMessage, target);
                    }, null, period, period);
                    break;
                case "stop":
                    Timer.Change(Timeout.Infinite, Timeout.Infinite);
                    PrimaryClient.SendMessage("Timer stopped.", originalMessage.Source);
                    break;
                case "execute":
                    var code = message.Substring(8);
                    object result;
                    bool resultSet;
                    Console.WriteLine(Evaluator.Evaluate(code, out result, out resultSet));
                    if (resultSet)
                        PrimaryClient.SendMessage(result.ToString(), originalMessage.Source);
                    break;
			}
		}

		void HandleUserJoinedChannel(object sender, ChannelUserEventArgs e)
		{
			if (SecondaryClients.Any(c => c.User.Nick == e.User.Nick)
			    || Program.Configuration.TrustedMasks.Any(m => e.User.Match(m)))
				e.Channel.ChangeMode("+o " + e.User.Nick);
			NickMaps[e.User.Nick] = e.User;
		}

		void HandleUserPartedChannel(object sender, ChannelUserEventArgs e)
		{
			if (NickMaps.ContainsKey(e.User.Nick))
				NickMaps.Remove(e.User.Nick);
		}

		void HandleUserKicked(object sender, KickEventArgs e)
		{
			if (!NickMaps.ContainsKey(e.Kicked.Nick) || !NickMaps.ContainsKey(e.Kicker.Nick))
				return;
			var kicked = NickMaps[e.Kicked.Nick];
			var kicker = NickMaps[e.Kicker.Nick];
			if (Program.Configuration.TrustedMasks.Any(m => kicked.Match(m)))
			{
				RespondTo(kicker, e.Channel);
				e.Channel.Invite(kicked.Nick);
			}
			var client = SecondaryClients.FirstOrDefault(c => c.User.Nick == kicked.Nick);
			if (client != null)
			{
				client.JoinChannel(e.Channel.Name);
				RespondTo(kicker, e.Channel);
			}
			if (kicked.Nick == PrimaryClient.User.Nick)
			{
				PrimaryClient.JoinChannel(e.Channel.Name);
				RespondTo(kicker, e.Channel);
			}
		}

		private void SetUpSecondaryEventHandlers(IrcClient client)
		{
			// Secondary clients only provide support to the primary client
			client.ConnectionComplete += HandleConnectionComplete;
			client.ModeChanged += HandleModeChanged;
			client.ChannelMessageRecieved += HandleChannelMessageRecieved;
			Logs[client.User.Nick] = new StreamWriter(client.User.Nick + ".log");
			client.RawMessageRecieved += (sender, e) => { Logs[(sender as IrcClient).User.Nick].WriteLine("<- " + e.Message); Logs[(sender as IrcClient).User.Nick].Flush(); };
			client.RawMessageSent += (sender, e) => { Logs[(sender as IrcClient).User.Nick].WriteLine("-> " + e.Message); Logs[(sender as IrcClient).User.Nick].Flush(); };
		}

		private void HandleModeChanged(object sender, ModeChangeEventArgs e)
		{
			var client = sender as IrcClient;
			if (e.Change.StartsWith("-o "))
			{
				var target = e.Change.Substring(3);
				var channel = client.Channels[e.Target];
				if (client.User.Nick == target || !channel.UsersByMode['o'].Contains(client.User.Nick))
					return; // Let another bot deal with it
				if (PrimaryClient.User.Nick == target)
				{
					var designatedClient = SecondaryClients.FirstOrDefault(c => channel.UsersByMode['o'].Contains(c.User.Nick));
					if (designatedClient == client)
					{
						channel.ChangeMode("+o " + PrimaryClient.User.Nick);
						RespondTo(e.User, channel);
					}
				}
				var affectedSecondaryClient = SecondaryClients.FirstOrDefault(c => c.User.Nick == target);
				if (affectedSecondaryClient != null)
				{
					channel.ChangeMode("+o " + affectedSecondaryClient.User.Nick);
					RespondTo(e.User, channel);
				}
				if (NickMaps.ContainsKey(target))
				{
					if (Program.Configuration.TrustedMasks.Any(m => NickMaps[target].Match(m)))
					{
						var designatedClient = SecondaryClients.FirstOrDefault(c => channel.UsersByMode['o'].Contains(c.User.Nick));
						if (channel.UsersByMode['o'].Contains(PrimaryClient.User.Nick))
							designatedClient = PrimaryClient;
						if (client == designatedClient)
						{
							channel.ChangeMode("+o " + target);
							RespondTo(e.User, channel);
						}
					}
				}
			}
			else if (e.Change.StartsWith("+b ") || e.Change.StartsWith("-I ") || e.Change.StartsWith("-e "))
			{
				string reverse = "-b ";
				if (e.Change.StartsWith("-I ") || e.Change.StartsWith("-e "))
					reverse = "+" + e.Change[1].ToString() + " ";
				var mask = e.Change.Substring(3);
				var channel = client.Channels[e.Target];
				var designatedClient = SecondaryClients.FirstOrDefault(c => channel.UsersByMode['o'].Contains(c.User.Nick));
				if (channel.UsersByMode['o'].Contains(PrimaryClient.User.Nick))
					designatedClient = PrimaryClient;
				if (mask.StartsWith("$a:"))
				{
					mask = mask.Substring(3);
					if (designatedClient == client)
					{
						var trusted = NickMaps.Where(nm => Program.Configuration.TrustedMasks.Any(t => nm.Value.Match(t))).ToArray();
						if (IrcUser.Match(mask, PrimaryClient.User.User) ||
							trusted.Any(t => IrcUser.Match(mask, t.Value.Nick)))
						{
							client.ChangeMode(channel.Name, reverse + "$a:" + mask);
							RespondTo(e.User, channel);
							return;
						}
					}
					return;
				}
				if (mask.StartsWith("$x:") || mask.StartsWith("$r"))
				{
					if (designatedClient == client)
						client.ChangeMode(channel.Name, reverse + mask);
					return;
				}
				var affectedUsers = NickMaps.Where(nm => nm.Value.Match(mask));
				if (designatedClient == client)
				{
					if (affectedUsers.Any())
					{
						foreach (var affectedUser in affectedUsers)
						{
							if (Program.Configuration.TrustedMasks.Any(m => affectedUser.Value.Match(m)) ||
								PrimaryClient.User.Match(mask) || SecondaryClients.Any(c => c.User.Match(mask)))
							{
								channel.ChangeMode(reverse + mask);
								RespondTo(e.User, channel);
								break;
							}
						}
					}
				}
			}
			else if (e.Change.StartsWith("+l "))
			{
				client.ChangeMode(e.Target, "-l");
			}
		}

		void RespondTo(IrcUser user, IrcChannel channel)
		{
			if (Program.Configuration.RespondAggressively)
			{
				if (Program.Configuration.TrustedMasks.Any(m => user.Match(m)))
					return;
				if (PrimaryClient.User.Nick == user.Nick)
					return;
				if (SecondaryClients.Any(c => c.User.Nick == user.Nick))
					return;
				try
				{
					PrimaryClient.GetModeList(channel.Name, 'e', mc =>
	                {
						channel.Kick(user.Nick, Program.Configuration.KickMessage);
						channel.ChangeMode("+b *!*@" + user.Hostname);
						while (mc.ContainsMatch(user))
						{
							var match = mc.GetMatch(user);
							channel.ChangeMode("-e " + match.Value);
							mc.Remove(match);
						}
					});
				}
				catch { }
			}
		}

		private void HandleConnectionComplete(object sender, EventArgs e)
		{
			var client = sender as IrcClient;
			Console.WriteLine("{0} successfully connected", client.User.Nick);
			var channels = Program.Configuration.Channels;
			if (client != PrimaryClient)
				channels = channels.Where(c => c.EnableMultiClient).ToArray();
			foreach (var channel in channels)
				client.JoinChannel(channel.Name);
			if (client == PrimaryClient)
			{
				// Clear to connect alternate clients
				for (int i = 1; i <= Program.Configuration.AlternateClients; i++)
				{
					var newClient = new IrcClient(Program.Configuration.IrcServer,new IrcUser(
						Program.Configuration.Identity.Nick + i, Program.Configuration.Identity.User, Program.Configuration.Identity.Password));
					SetUpSecondaryEventHandlers(newClient);
					SecondaryClients.Add(newClient);
					newClient.ConnectAsync();
				}
			}
		}
	}
}

