using System;

namespace Sirvant
{
	public class Configuration
	{
		public Configuration()
		{
			Identity = new Identity();
			Channels = new[] { new ChannelDescriptor() };
			TrustedMasks = new[] { "" };
			AlternateClients = 4;
			RespondAggressively = false;
		}

		public Identity Identity { get; set; }
		public ChannelDescriptor[] Channels { get; set; }
		public string KickMessage { get; set; }
		public string[] TrustedMasks { get; set; }
		public int AlternateClients { get; set; }
		public string IrcServer { get; set; }
		public bool RespondAggressively { get; set; }
	}

	public class Identity
	{
		public string Nick { get; set; }
		public string User { get; set; }
		public string Password { get; set; }
	}

	public class ChannelDescriptor
	{
		public string Name { get; set; }
		public bool EnableMultiClient { get; set; }
	}
}

