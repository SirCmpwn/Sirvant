using System;
using System.IO;
using Newtonsoft.Json;

namespace Sirvant
{
	public class Program
	{
		public static string ConfigurationFile { get; set; }
		public static Configuration Configuration { get; set; }
        public static BotController Controller { get; set; }

		public static void Main(string[] args)
		{
			ConfigurationFile = "config.json";
			if (args.Length == 1)
				ConfigurationFile = args[0];
			Configuration = new Configuration();
			if (!File.Exists(ConfigurationFile))
			{
				SaveConfiguration();
				Console.WriteLine("Generated new configuration file: {0}", ConfigurationFile);
				return;
			}
			Configuration = JsonConvert.DeserializeObject<Configuration>(File.ReadAllText(ConfigurationFile));
			Controller = new BotController();
			Controller.Run();

			Console.WriteLine("Press 'q' to quit");
            ConsoleKeyInfo cki;
            do
                cki = Console.ReadKey(true);
            while (cki.KeyChar != 'q');
		}

		public static void SaveConfiguration()
		{
			File.WriteAllText(ConfigurationFile, JsonConvert.SerializeObject(Configuration, Formatting.Indented));
		}
	}
}
