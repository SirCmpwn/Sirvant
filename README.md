# Sirvant

SirCmpwn's IRC servants. IRC bots that protect their owner from mischief, and do whatever else I tell them to.

This is mostly meant to deal with channel takeovers in #botwar on Freenode, which have become rather annoying
as of late. If you try to kick/ban/deop me or any of my bots, they'll make sure you can't do it twice. This
software runs several concurrent IRC bots (enough so they can't all be deopped in a single command) that work
together to deal with mean people.

If you want to use this software yourself, then just run `mono Sirvant.exe` to have a config file generated,
which you can then populate with the options you'd like. There are also a few commands that trusted users may
execute. You can use these by saying ".command [parameters]" in a channel (in which case, the primary bot
will handle it), or you can say "BotName: command [parameters]" to have a specific bot (including secondary
bots) handle it. You can also just PM a bot "command [parameters]" and the bot will execute the requested
command. The available commands include:

* addchannel [channel] [multiclient]: Adds the specified channel. multiclient is optional, and if true, all
  secondary clients will also join and work together with the primary client.
* removechannel: Removes the channel the command is issued from.
* addclients [number]: Spins up and connects an additional [number] alternate clients. Useful during severe
  channel takeovers. These are given the name of the primary client, plus a number. Make sure you've grouped
  a few alternate client nicks to the primary client's nickserv account, if your network supports it.
* removeclients [number]: Disconnects [number] alternate clients.
* killall: Disconnects all clients, including the primary client.
* listclients: Lists the nicks of all clients currently in use by the software.
* agressive: Toggles agressive responses. With agressive responses enabled, the bots will kickban any user
  that is unkind to them or a trusted user.
* join [channel]: Sometimes, it's possible for a bot to get left out of a channel during takeovers. Use this
  to join the channel again. Note that this does not modify the bot configuration, and just joins it during
  the current session.
